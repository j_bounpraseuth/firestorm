#!/bin/bash
cd ..

if [ ! -d "env" ]
then
	pip3 install virtualenv
	virtualenv -p python3 env
	source env/bin/activate
	pip install -r resources/requirements.txt
else
	source env/bin/activate
fi

# using pyqt gui
cd resources
python gui.py

# just run server
# cd webapp
# python manage.py runserver localhost:80