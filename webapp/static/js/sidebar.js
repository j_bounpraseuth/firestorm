// open sidebar
function openNav(option) {
	switch (option) {
   		case 1:
	  		if($('#sidenavUsers').width() != 350) {
				closeNav();
				$('#sidenavUsers').width(350);
				$('#map').width($(window).width()-415);
				$('#map').css('margin-left', 415); 
				$('#symbolUsers').addClass('active');
	  		} else {
				closeNav();
			}
			break;
		case 2:
	  		if($('#sidenavComments').width() != 350) {
				closeNav();
				$('#sidenavComments').width(350);
				$('#map').width($(window).width()-415);
				$('#map').css('margin-left', 415);
				$('#symbolComments').addClass('active');
	  		} else {
				closeNav();
			}
	  		break;
   		case 3:
	  		if($('#sidenavTablet').width() != 350) {
				closeNav();
				$('#sidenavTablet').width(350);
				$('#map').width($(window).width()-415);
				$('#map').css('margin-left', 415); 
				$('#symbolTablet').addClass('active');
	  		} else {
				closeNav();
			}
	  		break;
   		case 4:
	  		if($('#sidenavExclamation').width() != 350) {
				closeNav();
				$('#sidenavExclamation').width(350);
				$('#map').width($(window).width()-415);
				$('#map').css('margin-left', 415);
				$('#symbolExclamation').addClass('active');
	  		} else {
				closeNav();
			}    
	  		break;
   		default:
	  		closeNav()
 	}
}

// close sidebar
function closeNav() {
	$('#sidenavUsers').width(0);
	$('#symbolUsers').removeClass('active');

	$('#sidenavComments').width(0);
	$('#symbolComments').removeClass('active');

	$('#sidenavTablet').width(0);  
	$('#symbolTablet').removeClass('active');

	$('#sidenavExclamation').width(0);
	$('#symbolExclamation').removeClass('active');

	$('#map').width($(window).width()-65);
	$('#map').css('margin-left', 0);
}

// populate users and devices in sidemenu
function get_members(group, presence_data) {
	// add users and devices
	for (var i=0; i<presence_data.presenceList.presenceContact.length; i++) {
		if (presence_data.presenceList.presenceContact[i].presence.person.class.indexOf("user") != -1) {
			var duplicate = false;
			var presentityUserId = presence_data.presenceList.presenceContact[i].presentityUserId;
			var user_group = '<b>Group ID: </b>'+group;

			// check if the user is already in the sidebar
			$('.users').each(function() {	
				var id = $(this).attr('id');
				if (id.indexOf(presentityUserId) != -1) {
					duplicate = true;
					$(this).append('<br />'+user_group);
					$(this).attr('id',id+'_'+group);
				}
			});

			if (!duplicate) {
				var user = '<b>User Name: </b>'+presence_data.presenceList.presenceContact[i].presence.person.displayName;
				var user_id = presentityUserId+"_"+group;
				presentityUserId = '<b>User ID: </b>'+presentityUserId

				var item = '<a class="list-group-item list-group-item-action users" id="'+user_id+'">'+user+'<br />'+presentityUserId+'<br />'+user_group+'</a>';
				$('#users-div').append(item);
			}
		} else {
			var duplicate = false;
			var presentityUserId = presence_data.presenceList.presenceContact[i].presentityUserId;
			var device_group = '<b>Group ID: </b>'+group;

			// check if the device is already in the sidebar
			$('.devices').each(function() {
				var id = $(this).attr('id');
				if ($(this).attr('id').indexOf(presentityUserId) != -1) {
					duplicate = true;
					$(this).append('<br />'+device_group);
					$(this).attr('id',id+'_'+group);
				}
			});

			if (!duplicate) {
				var device = '<b>Device Name: </b>'+presence_data.presenceList.presenceContact[i].presence.person.displayName;
				var device_id = presentityUserId+"_"+group;
				presentityUserId = '<b>Device ID: </b>'+presentityUserId

				var item = '<a class="list-group-item list-group-item-action devices" id="'+device_id+'">'+device+'<br />'+presentityUserId+'<br />'+device_group+'</a>';
				$('#devices-div').append(item);		
			}		
		}
	}

	// check if the group is still selected
	$('.groups').each(function() {
		if(!$(this).hasClass('active')) {
			update_members($(this).attr('id'));
		}
	});

	// message user
	$('.users').each(function() {
		$(this).click(function(event) {
			var id = $(this).attr('id');
			id = id.substring(0,id.indexOf('_'));
			$('#receiver_id').val(id);

			var user = $(this).text();
			user = user.substring(11,user.indexOf('User ID'));

			$('#message_body').val('');
			$('#messaging_modal_title').text('Message: '+user);
			$('#messaging_modal').modal('show');
		});
	});

	$('#message_btn').click(function(event) {
		// messaging user using application token
		var receiver_id = $('#receiver_id').val();
		receiver = receiver_id.substring(receiver_id.indexOf(':')+1,receiver_id.indexOf('@'));

		var message_body = $('#message_body').val();
		var link_message = 'https://'+host+':65010/unsapi/messaging/1/'+agency_domain+'/app:'+client_id+'/outbox?apiKey='+api_key+'&access_token='+application_token;
		
		var xhttp = new XMLHttpRequest();
		xhttp.open('POST', proxy+link_message, true);
		xhttp.setRequestHeader('Content-type', 'application/json');

		// send a message from the application
		var message = '{"outboundMessageRequest": {'
				+'"address": [{"type": "one","jid":{"localpart": "'+receiver+'","domain":"'+agency_domain+'"}}],'
				+'"outboundXMPPMessage": {"body": [{"text": {"value": "'+message_body+'"}}]},'
				+'}}}'

		// message successfully sent
		xhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 201) {
				$('#success_modal').modal('show');
			}
		};

	    xhttp.send(message);
	})

	// zoom in on device
	$('.devices').each(function() {
		$(this).click(function(event) {
			var device = $(this).attr('id');
			device = device.substring(0,device.indexOf('_'));

			var found = false;
			for (i=0; i<markers_array.length; i++) {
				if (markers_array[i].marker.getTitle().indexOf(device) != -1) {
					found = true;
					var marker = markers_array[i].marker;
					var bounds = new google.maps.LatLngBounds();
					loc = new google.maps.LatLng(marker.position.lat(), marker.position.lng());
					bounds.extend(loc);
					map.fitBounds(bounds);
					map.panToBounds(bounds);
				}
			}

			if (!found) {
				$('#error_modal').modal('show');
			}
		});
	});

	// users search bar
	$('#search-users').on('keyup', function () {               		        	
		var searchText = $(this).val();
		searchText = searchText.toLowerCase();
		searchText = searchText.replace(/\s+/g, '');

		$('.users').each(function() {
			var currentGroupText = $(this).text(),      
			showCurrentGroup = ((currentGroupText.toLowerCase()).replace(/\s+/g, '')).indexOf(searchText) !== -1;
			$(this).toggle(showCurrentGroup);
		});  
	});

	// devices search bar
	$('#search-devices').on('keyup', function () {               		        	
		var searchText = $(this).val();
		searchText = searchText.toLowerCase();
		searchText = searchText.replace(/\s+/g, '');

		$('.devices').each(function() {
			var currentGroupText = $(this).text(),      
			showCurrentGroup = ((currentGroupText.toLowerCase()).replace(/\s+/g, '')).indexOf(searchText) !== -1;
			$(this).toggle(showCurrentGroup);
		});  
	});
}

// remove users and devices from sidemenu
function update_members(group) {
	$('.users').each(function() {
		if ($(this).attr('id').indexOf(group) != -1) {
			// update content
			var content = $(this).html();
			content = content.replace('<br><b>Group ID: </b>'+group,'');
			$(this).html(content);

			// update id
			var id = $(this).attr('id');
			id = id.replace('_'+group,'');
			$(this).attr('id',id);

			// check if it still part of a subscribed group
			if ($(this).text().indexOf('Group ID:') == -1) {
				$(this).remove();	
			}
		}
	});

	$('.devices').each(function() {
		if ($(this).attr('id').indexOf(group) != -1) {
			// update content
			var content = $(this).html();
			content = content.replace('<br><b>Group ID: </b>'+group,'');
			$(this).html(content);

			// update id
			var id = $(this).attr('id');
			id = id.replace('_'+group,'');
			$(this).attr('id',id);

			// check if it still part of a subscribed group
			if ($(this).text().indexOf('Group ID:') == -1) {
				$(this).remove();	
			}			
		}
	});
}

// get distressed devices
function distressed_devices() {
	// clear distressed devices list
	$('#distressed-devices-div').empty();

	// repopulate distressed devices list
	for (var i=0; i<markers_array.length; i++) {
		if (markers_array[i].emergency) {
			var title = markers_array[i].marker.getTitle();

			var presentity_id = title.substring(0,title.indexOf(' '));
			device = '<b>Device ID: </b>'+presentity_id;

			var groups = title.substring(title.indexOf(' ')+1);
			groups = groups.split(' ');
			var group = '<b>Group ID: </b>'+groups[0];
			
			for (var j=1; j<groups.length; j++) {
				group += '<br /><b>Group ID: </b>'+groups[j];
			}

			var item = '<a class="list-group-item list-group-item-action distressed-devices" id="'+presentity_id+'">'+device+'<br />'+group+'</a>';
			$('#distressed-devices-div').append(item);
		}
	}

	// if there are distressed devices, change symbol colour to red
	if ($.trim($("#distressed-devices-div").html()).length) {
		$('#symbolExclamation').css('color', '#e74c3c'); 
	} else {
		$('#symbolExclamation').css('color', '#005daa'); 
	}

	// on distressed device click, focus map on relevant marker
	$('.distressed-devices').each(function() {
		$(this).click(function(event) {
			var device = $(this).attr('id');
			for (i=0; i<markers_array.length; i++) {
				if (markers_array[i].marker.getTitle().indexOf(device) != -1) {
					var marker = markers_array[i].marker;
					var bounds = new google.maps.LatLngBounds();
					loc = new google.maps.LatLng(marker.position.lat(), marker.position.lng());
					bounds.extend(loc);
					map.fitBounds(bounds);
					map.panToBounds(bounds);
				}
			}
		});
	});

	// distressed devices search bar
	$('#search-distressed-devices').on('keyup', function () {               		        	
		var searchText = $(this).val();
		searchText = searchText.toLowerCase();
		searchText = searchText.replace(/\s+/g, '');

		$('.distressed-devices').each(function() {
			var currentGroupText = $(this).text(),      
			showCurrentGroup = ((currentGroupText.toLowerCase()).replace(/\s+/g, '')).indexOf(searchText) !== -1;
			$(this).toggle(showCurrentGroup);
		});  
	});
}

$('document').ready(function () {
	if (approval_needed) {
		$('#approval_modal').modal('show');	
	}

	// get all groups
	var link_groups = "https://"+host+":65000/unsapi/groupmgt/1/"+agency_domain+"/app:"+client_id+"/groups?apiKey="+api_key+"&access_token="+application_token;
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
    	if (this.readyState == 4 && this.status == 200) {
    		groups_data = JSON.parse(this.responseText);

		 	console.log("Groups Data");
    		console.log(groups_data.groupList.groupDescriptions);

		 	for (var i=0; i<groups_data.groupList.groupDescriptions.length; i++) {
		 		var id = groups_data.groupList.groupDescriptions[i].groupId;

		 		if (groups_data.groupList.groupDescriptions[i].displayName) {
		 			var display_name = '<b>Display Name: </b>'+groups_data.groupList.groupDescriptions[i].displayName;		 			
		 		} else {
		 			var display_name = '<b>Display Name: </b>';
		 		}

		 		if (groups_data.groupList.groupDescriptions[i].subject) {
		 			var subject = '<b>Subject: </b>'+groups_data.groupList.groupDescriptions[i].subject;
		 		} else {
		 			var subject = '<b>Subject: </b>';
		 		}

		 		var group_id = '<b>Group ID: </b>'+id;
			
				var item = '<a class="list-group-item list-group-item-action groups" id="'+id+'">'+display_name+'<br />'+subject+'<br />'+group_id+'</a>';
				$('#groups-div').append(item);
		 	}

			// select all groups button
			$('#select-all').click(function(event) {
				if ($(this).hasClass('active')) {
					$(this).removeClass('active');	
					$(".groups").each(function() {
						$(this).addClass('active');
						$(this).click();
					});
				} else {
					$(this).addClass('active');
					$(".groups").each(function() {
						$(this).removeClass('active');
						$(this).click();
					});
				}
			});

			// on group click create/destroy websocket for the group (add/delete group) 
			$('.groups').each(function() {
				$(this).click(function(event) {
					$(this).toggleClass('active');
					$(this).hasClass('active')?createWebsocket($(this).attr('id')):destroyWebsocket($(this).attr('id')); 
				});
			});

			// groups search bar
			$('#search-groups').on('keyup', function () {               		        	
				var searchText = $(this).val();
				searchText = searchText.toLowerCase();
				searchText = searchText.replace(/\s+/g, '');

				if ($.trim(searchText).length > 0) {
					$('#select-all').toggle(false);
				} else {
					$('#select-all').toggle(true);
				}

				$('.groups').each(function() {
					var currentGroupText = $(this).text(),      
					showCurrentGroup = ((currentGroupText.toLowerCase()).replace(/\s+/g, '')).indexOf(searchText) !== -1;
					$(this).toggle(showCurrentGroup);
				});  
			});
    	}
    };

  	xhttp.open("GET", proxy+link_groups, true);
	xhttp.send();
});