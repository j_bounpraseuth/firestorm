function get_presence(group,location_data,update_presence) {
	var link_token = "https://idmpsb.imw.motorolasolutions.com:9031/as/token.oauth2";
	
	var xhttp = new XMLHttpRequest();
	xhttp.open("POST", proxy+link_token, true);
	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
    		response = JSON.parse(this.responseText);
			new_application_token = response.access_token;

			// get presence data
			var link_presence = "https://"+host+":65000/unsapi/presence/1/"+agency_domain+"/"+group+"?apiKey="+api_key+"&access_token="+new_application_token;

			var xhttp = new XMLHttpRequest();
			xhttp.onreadystatechange = function() {
		    	if (this.readyState === 4 && this.status === 200) {
		    		presence_data = JSON.parse(this.responseText);

		    		marker_infowindow(location_data,presence_data);

		    		if (update_presence) {
		    			get_members(group, presence_data);
		    		}

				 	console.log("Presence Data");
				 	console.log(presence_data.presenceList.presenceContact);
		    	}
		    };

		  	xhttp.open("GET", proxy+link_presence, true);
			xhttp.send();
		}
    };

    xhttp.send("client_id="+client_id+"&client_secret="+client_secret+"&state=1234&grant_type=client_credentials&scope="+scope);
}

function marker_infowindow(location_data,presence_data) {
	for (var i=0; i<location_data.subscriptionNotification.terminalLocation.length; i++) {
		var device = location_data.subscriptionNotification.terminalLocation[i].address;
		var position = location_data.subscriptionNotification.terminalLocation[i].currentLocation.latitude+", "+location_data.subscriptionNotification.terminalLocation[i].currentLocation.longitude;
		var timestamp = location_data.subscriptionNotification.terminalLocation[i].currentLocation.timestamp.substring(0,10)+" "+location_data.subscriptionNotification.terminalLocation[i].currentLocation.timestamp.substring(11,16);

		for (var j=0; j<markers_array.length; j++) {
			// find the appropriate marker
		  	if (markers_array[j].marker.getTitle().indexOf(location_data.subscriptionNotification.terminalLocation[i].address) != -1) {	
				break;
			}
		}

		for (var k=0; k<presence_data.presenceList.presenceContact.length; k++) {
			// find the appropriate device
			if (presence_data.presenceList.presenceContact[k].presentityUserId.indexOf(device) != -1) {
				// set google maps marker info window content
				markers_array[j].infowindow.setContent(
					"Display Name: " + presence_data.presenceList.presenceContact[k].presence.person.displayName +
					'<div id="group_iw"></div>' +
					"Position: " + position +
					'<div id="address_iw"></div>' +
					"Device Class: " + presence_data.presenceList.presenceContact[k].presence.device[0].class +
					"<br /> Network ID: " + presence_data.presenceList.presenceContact[k].presence.device[0].networkAvailability.network[0].id +
					"<br /> Network Connection Status: " + presence_data.presenceList.presenceContact[k].presence.device[0].networkAvailability.network[0].connectionStatus +
					"<br /> Service Availability: " + presence_data.presenceList.presenceContact[k].presence.service[0].serviceAvailability +
					"<br /> Timestamp: " + timestamp
				);

				// set the marker to green if the device is active or grey if it inactive or red if it is in distress
				if (markers_array[j].emergency) {
					markers_array[j].marker.setIcon(red_marker);
				} else {
		    		if (presence_data.presenceList.presenceContact[k].presence.device[0].networkAvailability.network[0].connectionStatus.indexOf("Terminated") == -1) {
		    			markers_array[j].marker.setIcon(green_marker);
		    		} else {
		    			markers_array[j].marker.setIcon(grey_marker);	
		    		}
				}
				break;
			}
		}
	}
}

function marker_emergency(marker_object, emergency) {
	// update distressed devices
	if (emergency) {
		marker_object.emergency = true;
	} else {
		marker_object.emergency = false;	
	}

	distressed_devices();
}

function createWebsocket(group) {
	var presence_data;

	// imw location websocket url (subscription on a per group basis)
	link_location = "wss://"+host+":65001/unsapi/location/1/"+agency_domain+"/"+group+"?apiKey="+api_key+"&access_token="+application_token;

	// create a web socket
	var location_ws = new WebSocket(link_location);

	// when websocket receives data
	location_ws.onmessage = function(event) {
		var location_data = JSON.parse(event.data);
		var update_presence = false;

		// check if we already have a marker for the received data
	  	for (var i=0; i<location_data.subscriptionNotification.terminalLocation.length; i++) {
			var found = false;
			var position = new google.maps.LatLng(location_data.subscriptionNotification.terminalLocation[i].currentLocation.latitude, location_data.subscriptionNotification.terminalLocation[i].currentLocation.longitude);		
			var emergency = location_data.subscriptionNotification.terminalLocation[i].currentLocation.emergency;

			for (var j=0; j<markers_array.length; j++) {
				// if we have a marker, update its position
			  	if (markers_array[j].marker.getTitle().indexOf(location_data.subscriptionNotification.terminalLocation[i].address) != -1) {
					found = true;

					markers_array[j].marker.setPosition(position);

					// check if device is part of another group and update as necessary
					if (markers_array[j].marker.getTitle().indexOf(group) == -1) {
						update_presence = true;
						markers_array[j].marker.setTitle(markers_array[j].marker.getTitle()+" "+group);
					}

					// red marker if emergency flag is set
					marker_emergency(markers_array[j], emergency);
				}
			}

			// if we don't have a marker, create a new one
			if (!found) {
				update_presence = true;

				// create new marker
				var marker = new google.maps.Marker({
					icon: grey_marker,
					position: position,
					map: map,
					title: location_data.subscriptionNotification.terminalLocation[i].address+" "+group
				});

				// add info window to marker
				var infowindow = new google.maps.InfoWindow();

				infowindow.setContent("Failed to query presence data for this device.");

				// bind infowindow to marker
				google.maps.event.addListener(marker,'click', (function(marker,infowindow){ 
					return function() {
						if (openInfoWindow) {
							openInfoWindow.close();
						}

						// close infowindow if any part of the map is clicked
						google.maps.event.addListener(map, "click", function(event) {
							openInfoWindow.close();	
						});

			    		// convert latituate, longitude to addess
			    		var geocoder = new google.maps.Geocoder;
			    		var address = "";
			    		var latlng = {lat: marker.position.lat(), lng: marker.position.lng()};

			    		geocoder.geocode({'location': latlng}, function(results, status) {
						    if (status === 'OK') {
						    	if (results[0]) {
						        	address = results[0].formatted_address;
						    	} else {
						    		address = "Could not find address";
						    	}
						    } else {
						    	address = "Failed to get address";
						    }

						    $("#address_iw").text("Address: "+address);

						    // update groups the device is a part of
							var groups = marker.getTitle().substring(marker.getTitle().indexOf(" ")+1);
							groups = groups.split(" ");
							var group = groups[0];

							for (var i=1; i<groups.length; i++) {
								group += ", "+groups[i];
							}

							$("#group_iw").text("Groups: "+group);
						});
											
						openInfoWindow = infowindow;
						infowindow.open(map,marker);
					};
				})(marker,infowindow)); 

				// add marker and infowindow to array of markers
				markers_array.push({marker: marker, infowindow: infowindow, emergency: false});

				// red marker if emergency flag is set
				marker_emergency(markers_array[markers_array.length-1], emergency);

				// add marker to marker cluster and overlapping marker spiderfier
				oms.addMarker(marker);
				markerCluster.addMarker(marker);
				
			}
		}

		// get presence data
		get_presence(group,location_data, update_presence);

	  	// redraw marker clusters
	  	markerCluster.redraw();
	  
	  	// for debugging/logging purposes
	 	console.log("Location Data");
	 	console.log(location_data.subscriptionNotification.terminalLocation);
	}

	// push websocket to websocket array
	websockets.push({group: group, websocket: location_ws});
}

function destroyWebsocket(group) {	
	// update users sidebar
	update_members(group);
	
	// close websocket
	for (var i=0; i<websockets.length; i++) {
		if (group.localeCompare(websockets[i].group) == 0) {
			websockets[i].websocket.close();
			websockets.splice(i,1);
			i--;
		}
	}
	
	// remove relevant markers
	for (i=0; i<markers_array.length; i++) {
		if (markers_array[i].marker.getTitle().indexOf(group) != -1) {
			// check how many groups the device is in
			var marker_groups = markers_array[i].marker.getTitle().substring(markers_array[i].marker.getTitle().indexOf(" ")+1);
    		marker_groups = marker_groups.split(" ");

    		// update markers_array to reflect changes in groups
    		if (marker_groups.length == 1) {
				markerCluster.removeMarker(markers_array[i].marker);
				markers_array[i].marker.setMap(null);
				markers_array.splice(i,1);
				i--;    			
    		} else {
    			var marker_title = markers_array[i].marker.getTitle().replace(group, "");
    			marker_title = marker_title.replace(/  +/g, ' ');
    			marker_title = marker_title.trim();
    			markers_array[i].marker.setTitle(marker_title);
    		}
		}
	}

  	// redraw marker clusters
	markerCluster.resetViewport();
	markerCluster.redraw();

	// update distressed devices sidebar item
	distressed_devices();
}