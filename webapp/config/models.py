from django.db import models

# Create your models here.
class Setting(models.Model):
	name = models.CharField(max_length=100, default=None, null=True, blank=True)
	value = models.CharField(max_length=500, default=None, null=True, blank=True)

	def __str__(self):
		return '%s : %s' % (self.name, self.value)