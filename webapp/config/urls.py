from django.urls import path
from . import views

urlpatterns = [
	path('configuration/', views.configuration, name='configuration'),
	path('group_management/', views.group_management, name='group_management'),
]