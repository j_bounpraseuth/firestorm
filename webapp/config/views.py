from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from .models import Setting
import requests
import json

# Create your views here.
def configuration(request):
	# if form was submitted
	if request.method == 'POST':
		# function to save new configuration settings to the database
		def post(name_):
			namelower = name_.lower()
			namelower = namelower.replace(" ","_")
			t = Setting.objects.get(name=name_)
			t.value = request.POST[namelower]
			t.save()

		# check if an IMW token can be generated using the new configuration settings
		payload = {
			'client_id':request.POST['identity_service_application_username'],
			'client_secret':request.POST['identity_service_application_password'],
			'state':'1234',
			'grant_type':'client_credentials',
			'scope':'msi_unsapi_telemetry.watch msi_unsapi_groupmgt.read msi_unsapi_groupmgt.write msi_unsapi_presence.watch msi_unsapi_location.watch msi_unsapi_messaging'
		}

		url = 'https://'+request.POST['identity_service_host']+':9031/as/token.oauth2'
		r = requests.post(url, payload)

		if (r.status_code == 200):
			# successfully generated application token
			data = json.loads(r.content.decode('utf-8'))
			request.session['application_token'] = data['access_token']

			# delete the current tokens if they exists
			if 'user_token' in request.session:
				del request.session['user_token']
				del request.session['user_refresh_token']

			post('Google Maps API Key')
			post('URI Settings Developer ID')
			post('URI Settings License Key')
			post('URI Settings Agency Domain')
			post('URI Settings Host')
			post('Identity Service Host')
			post('Identity Service Application Password')
			post('Identity Service Application Username')

			return HttpResponseRedirect('/dashboard')
		else:
			# unsuccessfully generate application token
			request.session['google_maps_api_key'] = request.POST['google_maps_api_key']
			request.session['uri_settings_developer_id'] = request.POST['uri_settings_developer_id']
			request.session['uri_settings_license_key'] = request.POST['uri_settings_license_key']
			request.session['uri_settings_agency_domain'] = request.POST['uri_settings_agency_domain']
			request.session['uri_settings_host'] =request.POST['uri_settings_host']
			request.session['identity_service_host'] = request.POST['identity_service_host']
			request.session['identity_service_application_password'] = request.POST['identity_service_application_password']
			request.session['identity_service_application_username'] = request.POST['identity_service_application_username']
			return HttpResponseRedirect('/configuration/?error=True')

	else:
		# flag to display error message (i.e when application token cannot be generated from configuration settings)
		error = request.GET.get('error', '')

		# get IMW user token if it exists
		if 'user_token' in request.session:
			user_token = request.session['user_token']
			user_refresh_token = request.session['user_refresh_token']
			username = request.session['imw_user_username']
		else: 
			user_token = ''
			user_refresh_token = ''
			username = ''

		# set form values to previously used settings
		if error != '':
			google_maps_api_key = request.session['google_maps_api_key']
			uri_settings_developer_id = request.session['uri_settings_developer_id']
			uri_settings_license_key = request.session['uri_settings_license_key']
			uri_settings_agency_domain = request.session['uri_settings_agency_domain']
			uri_settings_host = request.session['uri_settings_host']
			identity_service_host = request.session['identity_service_host']
			identity_service_application_password = request.session['identity_service_application_password']
			identity_service_application_username = request.session['identity_service_application_username']
		else:
			google_maps_api_key = get_object_or_404(Setting.objects.filter(name__exact='Google Maps API Key')).value
			uri_settings_developer_id = get_object_or_404(Setting.objects.filter(name__exact='URI Settings Developer ID')).value
			uri_settings_license_key = get_object_or_404(Setting.objects.filter(name__exact='URI Settings License Key')).value
			uri_settings_agency_domain = get_object_or_404(Setting.objects.filter(name__exact='URI Settings Agency Domain')).value
			uri_settings_host = get_object_or_404(Setting.objects.filter(name__exact='URI Settings Host')).value
			identity_service_host = get_object_or_404(Setting.objects.filter(name__exact='Identity Service Host')).value
			identity_service_application_password = get_object_or_404(Setting.objects.filter(name__exact='Identity Service Application Password')).value
			identity_service_application_username = get_object_or_404(Setting.objects.filter(name__exact='Identity Service Application Username')).value

		return render(request,'configuration.html', {
			'client_id':get_object_or_404(Setting.objects.filter(name__exact='Identity Service Application Username')).value,
			'google_maps_api_key':google_maps_api_key,
			'uri_settings_developer_id':uri_settings_developer_id,
			'uri_settings_license_key':uri_settings_license_key,
			'uri_settings_agency_domain':uri_settings_agency_domain,
			'uri_settings_host':uri_settings_host,
			'identity_service_host':identity_service_host,
			'identity_service_application_password':identity_service_application_password,
			'identity_service_application_username':identity_service_application_username,
			'google_maps_api_key_rst':get_object_or_404(Setting.objects.filter(name__exact='Google Maps API Key')).value,
			'uri_settings_developer_id_rst':get_object_or_404(Setting.objects.filter(name__exact='URI Settings Developer ID')).value,
			'uri_settings_license_key_rst':get_object_or_404(Setting.objects.filter(name__exact='URI Settings License Key')).value,
			'uri_settings_agency_domain_rst':get_object_or_404(Setting.objects.filter(name__exact='URI Settings Agency Domain')).value,
			'uri_settings_host_rst':get_object_or_404(Setting.objects.filter(name__exact='URI Settings Host')).value,
			'identity_service_host_rst':get_object_or_404(Setting.objects.filter(name__exact='Identity Service Host')).value,
			'identity_service_application_password_rst':get_object_or_404(Setting.objects.filter(name__exact='Identity Service Application Password')).value,
			'identity_service_application_username_rst':get_object_or_404(Setting.objects.filter(name__exact='Identity Service Application Username')).value,
			'user_token':user_token,
			'username':username,
			'error':error,
		})

def group_management(request):
	# get IMW user token if it exists
	if 'user_token' in request.session:
		user_token = request.session['user_token']
		user_refresh_token = request.session['user_refresh_token']
		username = request.session['imw_user_username']
	else: 
		user_token = ''
		user_refresh_token = ''
		username = ''

	# check if we have an application token
	if 'application_token' in request.session:
		application_token = request.session['application_token']
	else:
		# generate IMW application token
		payload = {
			'client_id':get_object_or_404(Setting.objects.filter(name__exact='Identity Service Application Username')).value,
			'client_secret':get_object_or_404(Setting.objects.filter(name__exact='Identity Service Application Password')).value,
			'state':'1234',
			'grant_type':'client_credentials',
			'scope':'msi_unsapi_telemetry.watch msi_unsapi_groupmgt.read msi_unsapi_groupmgt.write msi_unsapi_presence.watch msi_unsapi_location.watch msi_unsapi_messaging'
		}

		url = 'https://'+get_object_or_404(Setting.objects.filter(name__exact='Identity Service Host')).value+':9031/as/token.oauth2'
		r = requests.post(url, payload)

		if (r.status_code == 200):
			# successfully generated application token
			data = json.loads(r.content.decode('utf-8'))
			request.session['application_token'] = data['access_token']
			application_token = data['access_token']
		else:
			# redirect back to page with error message
			return HttpResponseRedirect('/group_management/?status=application_token_false')

	# if form was submitted
	if request.method == 'POST':
		# which form was submitted
		group_management_request = request.GET.get('type', '')

		# get variables from database
		uri_settings_host = get_object_or_404(Setting.objects.filter(name__exact='URI Settings Host')).value
		uri_settings_agency_domain = get_object_or_404(Setting.objects.filter(name__exact='URI Settings Agency Domain')).value

		license_key = get_object_or_404(Setting.objects.filter(name__exact='URI Settings License Key')).value
		developer_id = get_object_or_404(Setting.objects.filter(name__exact='URI Settings Developer ID')).value
		api_key = developer_id + '.' + license_key
		status = ''

		# perform request as needed
		if group_management_request == 'create_group':
			group_name = request.POST['group_create']
			members = request.POST.getlist('members[]')
			members = members[:-1]

			# perform request
			body = {}
			body['group'] = {}
			body['group']['groupId'] = 'group:'+group_name+'@'+uri_settings_agency_domain
			body['group']['members'] = []

			for member in members:				
				body['group']['members'].append({
					'memberId': 'pres:'+member+'@'+uri_settings_agency_domain
				})

			url = 'https://'+uri_settings_host+':65000/unsapi/groupmgt/2/'+uri_settings_agency_domain+'/groups?apiKey='+api_key+'&access_token='+application_token
			r = requests.post(url, json=body)		
			status = r.status_code
		elif group_management_request == 'delete_group':
			group_name = request.POST['group_delete']

			# perform request
			url = 'https://'+uri_settings_host+':65000/unsapi/groupmgt/2/'+uri_settings_agency_domain+'/groups/group:'+group_name+'@'+uri_settings_agency_domain+'?apiKey='+api_key+'&access_token='+application_token
			r = requests.delete(url)
			status = r.status_code
		elif group_management_request == 'display_name':
			group_name = request.POST['group_display_name']
			display_name = request.POST['display_name']

			# perform request
			body = {}
			body['displayname'] = display_name

			url = 'https://'+uri_settings_host+':65000/unsapi/groupmgt/2/'+uri_settings_agency_domain+'/groups/group:'+group_name+'@'+uri_settings_agency_domain+'/displayname?apiKey='+api_key+'&access_token='+application_token
			r = requests.put(url, json=body) 
			status = r.status_code
		elif group_management_request == 'subject':
			group_name = request.POST['subject_group']
			subject = request.POST['subject']

			# perform request
			body = {}
			body['subject'] = subject

			url = 'https://'+uri_settings_host+':65000/unsapi/groupmgt/2/'+uri_settings_agency_domain+'/groups/group:'+group_name+'@'+uri_settings_agency_domain+'/subject?apiKey='+api_key+'&access_token='+application_token
			r = requests.put(url, json=body)
			status = r.status_code
		elif group_management_request == 'add_member':
			group_name = request.POST['member_add_group']
			member_name = request.POST['member_add']

			# perform request
			body = {}
			body['groupMember'] = {}
			body['groupMember']['memberId'] = 'pres:'+member_name+'@'+uri_settings_agency_domain

			url = 'https://'+uri_settings_host+':65000/unsapi/groupmgt/2/'+uri_settings_agency_domain+'/groups/group:'+group_name+'@'+uri_settings_agency_domain+'/members?apiKey='+api_key+'&access_token='+application_token
			r = requests.post(url, json=body)
			status = r.status_code
		elif group_management_request == 'delete_member':
			group_name = request.POST['member_delete_group']
			member_name = request.POST['member_delete']	

			#perform request
			url = 'https://'+uri_settings_host+':65000/unsapi/groupmgt/2/'+uri_settings_agency_domain+'/groups/group:'+group_name+'@'+uri_settings_agency_domain+'/members/pres:'+member_name+'@'+uri_settings_agency_domain+'?apiKey='+api_key+'&access_token='+application_token
			r = requests.delete(url)
			status = r.status_code
		else:
			status = unknown

		return HttpResponseRedirect('/group_management/?status='+str(status))
	else:
		# get error messages if any
		status = request.GET.get('status', '')

		# create IMW api key
		license_key = get_object_or_404(Setting.objects.filter(name__exact='URI Settings License Key')).value
		developer_id = get_object_or_404(Setting.objects.filter(name__exact='URI Settings Developer ID')).value
		api_key = developer_id + '.' + license_key

		return render(request,'group_management.html', {
			'client_id':get_object_or_404(Setting.objects.filter(name__exact='Identity Service Application Username')).value,
			'client_secret':get_object_or_404(Setting.objects.filter(name__exact='Identity Service Application Password')).value,
			'host':get_object_or_404(Setting.objects.filter(name__exact='URI Settings Host')).value,
			'agency_domain':get_object_or_404(Setting.objects.filter(name__exact='URI Settings Agency Domain')).value,
			'idm_host':get_object_or_404(Setting.objects.filter(name__exact='Identity Service Host')).value,
			'api_key':api_key,
			'application_token':application_token,
			'user_token':user_token,
			'username':username,
			'status':status,
		})