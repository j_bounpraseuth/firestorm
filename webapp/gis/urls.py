from django.urls import path
from . import views

urlpatterns = [
	path('', views.user_token, name='user_token'),
	path('logout/', views.logout, name='logout'),
	path('dashboard/', views.dashboard, name='dashboard'),
]