from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from config.models import Setting
import requests
import json

# Create your views here.
def user_token(request):
	code = request.GET.get('code', '')
	if code != '':
		# get IMW user token
		payload = {
			'client_id':get_object_or_404(Setting.objects.filter(name__exact='Identity Service Application Username')).value,
			'client_secret':get_object_or_404(Setting.objects.filter(name__exact='Identity Service Application Password')).value,
			'state':'1234',
			'redirect_uri':'http://localhost/',
			'grant_type':'authorization_code',
			'scope':'msi_unsapi_telemetry.watch msi_unsapi_groupmgt.read msi_unsapi_groupmgt.write msi_unsapi_presence.watch msi_unsapi_location.watch msi_unsapi_messaging',
			'code':code
		}

		url = 'https://'+get_object_or_404(Setting.objects.filter(name__exact='Identity Service Host')).value+':9031/as/token.oauth2'
		r = requests.post(url, payload)
		data = json.loads(r.content.decode('utf-8'))
		token = data['access_token']
		refresh_token = data['refresh_token']

		request.session['user_token'] = token
		request.session['user_refresh_token'] = refresh_token

	return HttpResponseRedirect('/dashboard')

def logout(request):
	# delete user token and associated variables
	del request.session['imw_user_username']
	del request.session['imw_user_password']
	del request.session['user_token']
	del request.session['user_refresh_token']
	return HttpResponseRedirect('/dashboard')

def dashboard(request):
	approval_needed = False
	approval_url = ''

	# if form was submitted
	if request.method == 'POST':
		request.session['imw_user_username'] = request.POST['user_token_username']
		request.session['imw_user_password'] = request.POST['user_token_password']

		# get IMW user code
		data = {
			'pf.username':request.session['imw_user_username'],
			'pf.pass':request.session['imw_user_password']
		}

		url = 'https://'+get_object_or_404(Setting.objects.filter(name__exact='Identity Service Host')).value+':9031/as/authorization.oauth2?client_id='+get_object_or_404(Setting.objects.filter(name__exact='Identity Service Application Username')).value+'&response_type=code&redirect_uri=http://localhost/&scope=msi_unsapi_telemetry.watch msi_unsapi_groupmgt.read msi_unsapi_groupmgt.write msi_unsapi_presence.watch msi_unsapi_location.watch msi_unsapi_messaging'
		s = requests.Session()
		req = s.post(url, data=data, allow_redirects=False)

		# request for approval needed
		if (req.status_code == 200):
			approval_needed = True
			approval_url = url
		# valid login credentials
		elif (req.status_code == 302):
			code_url = req.headers['Location']
			code = code_url[code_url.index("=")+1:]

			# get IMW user token
			payload = {
				'client_id':get_object_or_404(Setting.objects.filter(name__exact='Identity Service Application Username')).value,
				'client_secret':get_object_or_404(Setting.objects.filter(name__exact='Identity Service Application Password')).value,
				'state':'1234',
				'redirect_uri':'http://localhost/',
				'grant_type':'authorization_code',
				'scope':'msi_unsapi_telemetry.watch msi_unsapi_groupmgt.read msi_unsapi_groupmgt.write msi_unsapi_presence.watch msi_unsapi_location.watch msi_unsapi_messaging',
				'code':code
			}

			url = 'https://'+get_object_or_404(Setting.objects.filter(name__exact='Identity Service Host')).value+':9031/as/token.oauth2'
			r = requests.post(url, payload)
			data = json.loads(r.content.decode('utf-8'))
			token = data['access_token']
			refresh_token = data['refresh_token']

			request.session['user_token'] = token
			request.session['user_refresh_token'] = refresh_token

	# get IMW user token if it exists
	if 'user_token' in request.session:
		user_token = request.session['user_token']
		user_refresh_token = request.session['user_refresh_token']
		username = request.session['imw_user_username']
	else: 
		user_token = ''
		user_refresh_token = ''
		username = ''

	# get IMW application token if it exists
	if 'application_token' in request.session:
		application_token = request.session['application_token']
	else:
		# generate IMW application token
		payload = {
			'client_id':get_object_or_404(Setting.objects.filter(name__exact='Identity Service Application Username')).value,
			'client_secret':get_object_or_404(Setting.objects.filter(name__exact='Identity Service Application Password')).value,
			'state':'1234',
			'grant_type': 'client_credentials',
			'scope':'msi_unsapi_telemetry.watch msi_unsapi_groupmgt.read msi_unsapi_groupmgt.write msi_unsapi_presence.watch msi_unsapi_location.watch msi_unsapi_messaging',
		}

		url = 'https://'+get_object_or_404(Setting.objects.filter(name__exact='Identity Service Host')).value+':9031/as/token.oauth2'
		r = requests.post(url, payload)

		if (r.status_code == 200):
			# successfully generated application token
			data = json.loads(r.content.decode('utf-8'))
			request.session['application_token'] = data['access_token']
			application_token = data['access_token']
		else:
			application_token = ''

	# create IMW api key
	license_key = get_object_or_404(Setting.objects.filter(name__exact='URI Settings License Key')).value
	developer_id = get_object_or_404(Setting.objects.filter(name__exact='URI Settings Developer ID')).value
	api_key = developer_id + '.' + license_key

	# render html
	return render(request,'dashboard.html', {
		'google_maps_api_key':get_object_or_404(Setting.objects.filter(name__exact='Google Maps API Key')).value,
		'client_id':get_object_or_404(Setting.objects.filter(name__exact='Identity Service Application Username')).value,
		'client_secret':get_object_or_404(Setting.objects.filter(name__exact='Identity Service Application Password')).value,
		'host':get_object_or_404(Setting.objects.filter(name__exact='URI Settings Host')).value,
		'agency_domain':get_object_or_404(Setting.objects.filter(name__exact='URI Settings Agency Domain')).value,
		'idm_host':get_object_or_404(Setting.objects.filter(name__exact='Identity Service Host')).value,
		'application_token':application_token,
		'user_token':user_token,
		'api_key':api_key,
		'username':username,
		'approval_needed':approval_needed,
		'approval_url':approval_url,
	})