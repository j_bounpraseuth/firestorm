from django import template
from django.template.defaultfilters import stringfilter

register = template.Library()

@register.filter
@stringfilter
def group_name(value):
    return value[value.index(':')+1:value.index('@')]