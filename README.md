# README #
The Firestorm web application is a 3rd party application that demonstrates the capabilities and effectiveness of the IMW APIs.

To run the Firestorm application on Windows, upzip the py-dist.zip file and run the firestorm.bat file.
To run the Firestorm application on Mac OS or Linux, navigate to the resources folder and run the firestorm.sh script.
The rest of the readme contains information concerning the development of the Firestorm application.

## Features of the Firestorm Appliation ##
* Displaying the locations, presence and identities of both LTE and LMR devices on a GIS map
* Displaying all groups on the IMW Server that are accessible by the IMW application user
* Sending messages to users logged into PSX Cockpit (on LEX L10 Devices)
* The ability to connect to any IMW Server (given the correct configuration details)
* Creating and deleting ad-hoc groups
* Adding and removing users and devices from groups
* Configuring the subject name and display name of groups
* Logging in using an IMW user account (for the purpose of duplex messaging)
* The ability to be run on any OS (Windows, Mac or Linux) or even hosted as a website

## Features Still to be Added ##
* Duplex messaging using XMPP. Currently only simplex messaging is supported (application to device)

## THE FOLLOWING ONLY APPLIES IF YOU ARE MANUALLY COMPILING THE WEBAPP ##

### Login to Django Admin (http://localhost/admin/) ###

* Username: admin
* Password: Motorola1

### cors-anywhere ###

* https://github.com/Rob--W/cors-anywhere
* cors-anywhere is a NodeJS proxy which adds CORS headers to the proxied request.
* It is used to send HTTP GET/POST reqeusts from the applications frontend (in javascript).

### Installation of virtualenv and dependencies ###

Instructions on how to set up and compile the Firestorm django application. Note that we are using python3.
To check which version of python you are running, run the command:
`python --version`

* Run the following commands to download virtualenv and required dependencies.
* virtualenv creates a 'virtual environment' (called env) in which we will install all our packages. 
* This will allow us to maintain packages installed via pip (known as dependencies).

```Shell
cd firestorm
pip install virtualenv
virtualenv env
env\Scripts\activate
pip install -r requirements.txt
```

### Running the webserver (locally) ###

* Make sure the virtual environment is running.
`\path\to\env\Scripts\activate` on Windows OR `source /path/to/env/bin/activate` on MacOS.
* Navigate to the 'webapp' directory (which contains 'manage.py') and run the following command as administrator.
* On Windows, run command prompt as administrator. On MacOS, use sudo and specify the path to python.
`sudo /path/to/firestorm/env/bin/python manage.py runserver localhost:80`
OR
`python manage.py runserver localhost:80`
* Go to the website listed (http://localhost:80/) to view the Firestorm webapp.

### Notes ###

* If you install packages using pip, run the following command to update the 'requirements.txt' file. Make sure you are in the same directory as 'requirements.txt'.
`pip freeze > requirements.txt`